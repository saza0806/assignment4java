package com.company;

public class Student {
    private static final int NUM_EXAMS = 4;

    private String firstName;
    private String lastName;
    private int gradeLevel;
    private double gpa;



    private int[] exams;
    private int numExamsTaken;

    public Student(String fName, String lName, int grade)
    {
        firstName = fName;
        lastName = lName;
        gradeLevel = grade;
        exams = new int[NUM_EXAMS];
        numExamsTaken = 0;
    }
    public double getGroupStats() //this is the method that I need to, but I'm not sure if it is even correct.
    {
        int z=0;
        for(int i =0; i<exams.length; i++)
        {
            z+=exams[i];
        }
        return z/(double) numExamsTaken;
    }

    public String getName()
    {
        return firstName + " " + lastName;
    }
}
